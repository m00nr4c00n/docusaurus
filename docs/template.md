---
id: documentID
title: documentTitle
sidebar_label: documentSideLable
---

## Anchor Heading

`Code snippet`

~~~
Code block
for easier reading
~~~

```Bash
info = "color highlighted language specific code block"
    while read -r i
    do
        echo $info + $i 
    done 
    < textfile.txt

exit 1
```
# Tables

| header 1 | header 2 | header 3 |
| ---      |  ------  |----------|
| cell 1   | cell 2   | cell 3   |
