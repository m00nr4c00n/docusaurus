---
id: gitlab
title: Setup SSH to gitlab
sidebar_label: gitlab SSH setup
---

# setup SSH key:
```bash
ssh-keygen -t rsa -b 4096 -C "USER@mail.com"
```
cat the public key and paste it into gitlabs  

# test the key against git:
```bash
ssh -i id_rsa_gitlab -T git@gitlab.com
```
should return an OK message from gitlab  

# specify default path to key file:
```bash
eval $(ssh-agent -s)
ssh-add ~/.ssh/other_id_rsa
```
setup config file in .ssh/config (dont forget to set permissions correctly)  
```bash
nano ~/.ssh/config
# GitLab.com
Host gitlab.com
  Preferredauthentications publickey
  IdentityFile ~/.ssh/gitlab_com_rsa
```
# Set permission:
```bash
chown USER:USER /home/USER/.ssh/config
chmod 644 /home/USER/.ssh/config
```
# Configure git username and email, then verify them:
```bash
git config --global user.name "USER"
git config --global user.email "USER@mail.com"

git config --global user.name
git config --global user.email
```
# Clone gitlab repository over ssh:
```bash
git clone ssh://git@gitlab.com/USER/repo
```
# Update git repository with local changes (from the repository root):
```bash
git add .
git commit -m "Comment to describe the changes made of the commit"

git push origin master
```
for more info read - https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
