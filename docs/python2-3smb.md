---
id: python2-3
title: python2-3 pip
sidebar_label: python2-3 pip fix
---


To get python 2 and 3 to work with pip install and more particularly SMB and impacket

Fix (assuming you have both python 2 and 3 installed, check with python -V):

    Uninstall pip3: https://installlion.com/kali/kali/main/p/python3-pip/uninstall/index.html
    Get pip3 installer: curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    Install pip3: python3 get-pip.py
    Get pip2 installer: curl https://bootstrap.pypa.io/2.7/get-pip.py -o get-pip.py
    Install pip2: python get-pip.py
    Now you can install any pip2 modules: pip install pyasn1 pycrypto pycryptodome pycryptodomex

after that it should work to run python pip install [module](for python 2.7) and python3 pip install [module]  (for python3)
